import React from 'react'
import './App.css'
import HeaderPokedex from '../src/components/HeaderPokedex'
import FooterPokedex from '../src/components/FooterPokedex'
import Pokemon from '../src/components/Pokemon'
import { useEffect, useState } from 'react'

export default function App() {
	const [getPokedex, setPokedex] = useState([])
	const pokemonRandomId = useState(Math.floor(Math.random() * 800 + 1))


	function listarPokemons(result){
		result.forEach(async(pokemon) => {
		  const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${pokemon.name}`)
		  const data = await res.json()
		  setPokedex(currentList => [...currentList,data])
		})
	}

	const obtenerPokemons = async () => {
		const res = await fetch(`https://pokeapi.co/api/v2/pokemon?limit=5&offset=${pokemonRandomId}`)
		const data = await res.json()
		   
		listarPokemons(data.results)
		console.log(data.results)
	}

	useEffect(() => {
		obtenerPokemons()
	}, [])

	

	return (

		<div className="contenedor">
			<HeaderPokedex />
			<section className="main">
			{
				getPokedex.map((pokemon,index) =>{
                 
					return <Pokemon
						id={pokemon.id}
						name={pokemon.name}
						image={pokemon.sprites.other.dream_world.front_default}
						type={pokemon.types[0].type.name}
						ability={pokemon.abilities[0].ability.name}
						level={pokemon.base_experience}
						height={pokemon.height}
						weight={pokemon.weight}
						key = {index}
					/>})[0]	
			}				
			</section>
	        <footer>
			<div className="cards_title">others</div>
			<div className="cards">
			{
				getPokedex.map((pokemon,index) =>{
                  if(index>0)
					return <FooterPokedex
						name={pokemon.name}
						image={pokemon.sprites.other.dream_world.front_default}
						type={pokemon.types[0].type.name}
						key = {index}
					/>})
			}	
			</div>	
			</footer>
			
		</div>
	)

}

