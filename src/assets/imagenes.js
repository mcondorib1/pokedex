import bug from './img/bug.svg';
import dark from './img/dark.svg'
import dragon from './img/dragon.svg'
import electric from './img/electric.svg'
import fairy from './img/fairy.svg'
import fighting from './img/fighting.svg'
import fire from './img/fire.svg'
import flying from './img/flying.svg'
import ghost from './img/ghost.svg'
import grass from './img/grass.svg'
import ground from './img/ground.svg'
import ice from './img/ice.svg'
import normal from './img/normal.svg'
import poison from './img/poison.svg'
import psychic from './img/psychic.svg'
import rock from './img/rock.svg'
import steel from './img/steel.svg'
import water from './img/water.svg'
import stats from './img/stats.png'


export default [
    { "tipo": bug },
    { "tipo": dark },
    { "tipo": dragon },
    { "tipo": electric },
    { "tipo": fairy },
    { "tipo": fighting },
    { "tipo": fire },
    { "tipo": flying },
    { "tipo": ghost },
    { "tipo": grass },
    { "tipo": ground },
    { "tipo": ice },
    { "tipo": normal },
    { "tipo": poison },
    { "tipo": psychic },
    { "tipo": rock },
    { "tipo": steel },
    { "tipo": water },
    { "tipo": stats }
]