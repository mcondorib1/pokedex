import React from 'react';
import imagenes from '../assets/imagenes'

let fill = (number, len) => "0".repeat(len - number.toString().length) + number.toString()

const Pokemon = ({ id, name, image, type, level, ability, height, weight }) => {

   let ind = 0

   if (type === 'bug') { ind = 0; }
   else if (type === 'dark') { ind = 1 }
   else if (type === 'dragon') { ind = 2 }
   else if (type === 'electric') { ind = 3 }
   else if (type === 'fairy') { ind = 4 }
   else if (type === 'fighting') { ind = 5 }
   else if (type === 'fire') { ind = 6 }
   else if (type === 'flying') { ind = 7 }
   else if (type === 'ghost') { ind = 8 }
   else if (type === 'grass') { ind = 9 }
   else if (type === 'ground') { ind = 10 }
   else if (type === 'ice') { ind = 11 }
   else if (type === 'normal') { ind = 12 }
   else if (type === 'poison') { ind = 13 }
   else if (type === 'psychic') { ind = 14 }
   else if (type === 'rock') { ind = 15 }
   else if (type === 'steel') { ind = 16 }
   else if (type === 'water') { ind = 17 }
   else if (type === 'sats') { ind = 18 }
   else { ind = 19 }

   return (
      <div style={{ 
       display: 'flex',   
       flexDirection: 'row',
       flexWrap: 'wrap',
       }}>
         <div className="main-left">
            <div className="nombre">
               <img className="stats" src={imagenes[18].tipo} alt="" /><h3>{name}</h3>
            </div>
            <div
               style={{
                  position: 'relative',
                  backgroundSize: '42%',
                  backgroundPosition: 'right',
                  backgroundRepeat: 'no-repeat',
                  backgroundImage: `url(${imagenes[ind].tipo})`
               } }
            >
               <img className="pokemon" src={image} alt={name} />
               <div className="pokemon2"></div>
            </div>
         </div>
         <div className="main-right">
            <div className="stats_panel">
               <div className="fila"><div className="statname">No</div><div className="statvalue">{fill(id, 3)}</div></div>
               <div className="fila"><div className="statname">level</div><div className="statvalue">{level}</div></div>
            </div>
            <div className="stats_panel">
               <div className="fila"><div className="statname">type</div><div className="statvalue">{type} </div></div>
               <div className="fila"><div className="statname">hability</div><div className="statvalue">{ability}</div></div>
            </div>
            <div className="stats_panel">
               <div className="fila"><div className="statname">height</div><div className="statvalue">{height}</div></div>
               <div className="fila"><div className="statname">weight</div><div className="statvalue">{weight}</div></div>
            </div>
         </div>

      </div>


   )
}

export default Pokemon