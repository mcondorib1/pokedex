import React from 'react'
import imagenes from '../assets/imagenes'

const FooterPokedex = (({ name, image,type}) => {
  let ind = 0

  if (type === 'bug') { ind = 0; }
  else if (type === 'dark') { ind = 1 }
  else if (type === 'dragon') { ind = 2 }
  else if (type === 'electric') { ind = 3 }
  else if (type === 'fairy') { ind = 4 }
  else if (type === 'fighting') { ind = 5 }
  else if (type === 'fire') { ind = 6 }
  else if (type === 'flying') { ind = 7 }
  else if (type === 'ghost') { ind = 8 }
  else if (type === 'grass') { ind = 9 }
  else if (type === 'ground') { ind = 10 }
  else if (type === 'ice') { ind = 11 }
  else if (type === 'normal') { ind = 12 }
  else if (type === 'poison') { ind = 13 }
  else if (type === 'psychic') { ind = 14 }
  else if (type === 'rock') { ind = 15 }
  else if (type === 'steel') { ind = 16 }
  else if (type === 'water') { ind = 17 }
  else if (type === 'sats') { ind = 18 }
  else { ind = 19 }
  return (
      
        <a href={() => false} className="nada "><img style={{
        alignSelf:'center',
        height: '170px',
        position: 'flex',
        mozBorderRadius: '50%',
        webkitBorderRadius: '50%',
        borderRadius: '50%',
        background: '#fff',
        width:'170px',
        alignItems:'center',
        backgroundSize: '100%',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundImage: `url(${imagenes[ind].tipo})`,
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap'
      }} src={image} alt={name} /></a>
    
  )
})

export default FooterPokedex