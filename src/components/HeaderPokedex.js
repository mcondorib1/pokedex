import React from 'react';

const HeaderPokedex = () => {
  return (

    <header>
      <div className="nombre_header">
        <a href={() => false} className="logo"><img src="img/logo.png" alt="" /></a>
        <h3 >pokedex</h3>
      </div>
    </header>

  )
}

export default HeaderPokedex